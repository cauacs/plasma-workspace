# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-22 00:41+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: tok\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: package/contents/config/config.qml:20
#, kde-format
msgid "Appearance"
msgstr ""

#: package/contents/config/config.qml:25
#, kde-format
msgid "Calendar"
msgstr ""

#: package/contents/config/config.qml:30
#: package/contents/ui/CalendarView.qml:422
#, kde-format
msgid "Time Zones"
msgstr ""

#: package/contents/ui/CalendarView.qml:144
#, kde-format
msgid "Events"
msgstr ""

#: package/contents/ui/CalendarView.qml:153
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr ""

#: package/contents/ui/CalendarView.qml:157
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr ""

#: package/contents/ui/CalendarView.qml:382
#, kde-format
msgid "No events for today"
msgstr ""

#: package/contents/ui/CalendarView.qml:383
#, kde-format
msgid "No events for this day"
msgstr ""

#: package/contents/ui/CalendarView.qml:433
#, kde-format
msgid "Switch…"
msgstr ""

#: package/contents/ui/CalendarView.qml:436
#: package/contents/ui/CalendarView.qml:437
#, kde-format
msgid "Switch to another time zone"
msgstr ""

#: package/contents/ui/configAppearance.qml:51
#, kde-format
msgid "Information:"
msgstr ""

#: package/contents/ui/configAppearance.qml:56
#, kde-format
msgid "Show date"
msgstr ""

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgid "Adaptive location"
msgstr ""

#: package/contents/ui/configAppearance.qml:65
#, kde-format
msgid "Always beside time"
msgstr ""

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgid "Always below time"
msgstr ""

#: package/contents/ui/configAppearance.qml:74
#, kde-format
msgid "Show seconds:"
msgstr ""

#: package/contents/ui/configAppearance.qml:76
#, kde-format
msgctxt "@option:check"
msgid "Never"
msgstr ""

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgctxt "@option:check"
msgid "Only in the tooltip"
msgstr ""

#: package/contents/ui/configAppearance.qml:78
#: package/contents/ui/configAppearance.qml:99
#, kde-format
msgid "Always"
msgstr ""

#: package/contents/ui/configAppearance.qml:88
#, kde-format
msgid "Show time zone:"
msgstr ""

#: package/contents/ui/configAppearance.qml:94
#, kde-format
msgid "Only when different from local time zone"
msgstr ""

#: package/contents/ui/configAppearance.qml:110
#, kde-format
msgid "Display time zone as:"
msgstr ""

#: package/contents/ui/configAppearance.qml:113
#, kde-format
msgid "Code"
msgstr ""

#: package/contents/ui/configAppearance.qml:114
#, kde-format
msgid "City"
msgstr ""

#: package/contents/ui/configAppearance.qml:115
#, kde-format
msgid "Offset from UTC time"
msgstr ""

#: package/contents/ui/configAppearance.qml:126
#, kde-format
msgid "Time display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:132
#, kde-format
msgid "12-Hour"
msgstr ""

#: package/contents/ui/configAppearance.qml:133
#: package/contents/ui/configCalendar.qml:55
#, kde-format
msgid "Use Region Defaults"
msgstr ""

#: package/contents/ui/configAppearance.qml:134
#, kde-format
msgid "24-Hour"
msgstr ""

#: package/contents/ui/configAppearance.qml:141
#, kde-format
msgid "Change Regional Settings…"
msgstr ""

#: package/contents/ui/configAppearance.qml:152
#, kde-format
msgid "Date format:"
msgstr ""

#: package/contents/ui/configAppearance.qml:161
#, kde-format
msgid "Long Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:168
#, kde-format
msgid "Short Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:175
#, kde-format
msgid "ISO Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:182
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr ""

#: package/contents/ui/configAppearance.qml:212
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""

#: package/contents/ui/configAppearance.qml:237
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:238
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr ""

#: package/contents/ui/configAppearance.qml:242
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr ""

#: package/contents/ui/configAppearance.qml:254
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr ""

#: package/contents/ui/configAppearance.qml:264
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr ""

#: package/contents/ui/configAppearance.qml:277
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr ""

#: package/contents/ui/configAppearance.qml:285
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr ""

#: package/contents/ui/configCalendar.qml:41
#, kde-format
msgid "General:"
msgstr ""

#: package/contents/ui/configCalendar.qml:42
#, kde-format
msgid "Show week numbers"
msgstr ""

#: package/contents/ui/configCalendar.qml:49
#, kde-format
msgid "First day of week:"
msgstr ""

#: package/contents/ui/configCalendar.qml:72
#, kde-format
msgid "Available Plugins:"
msgstr ""

#: package/contents/ui/configTimeZones.qml:42
#, kde-format
msgid ""
"Tip: if you travel frequently, add your home time zone to this list. It will "
"only appear when you change the systemwide time zone to something else."
msgstr ""

#: package/contents/ui/configTimeZones.qml:59
#, kde-format
msgid "Add Time Zones…"
msgstr ""

#: package/contents/ui/configTimeZones.qml:93
#, kde-format
msgid "Clock is currently using this time zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:95
#, kde-format
msgctxt ""
"@label This list item shows a time zone city name that is identical to the "
"local time zone's city, and will be hidden in the time zone display in the "
"plasmoid's popup"
msgid "Hidden while this is the local time zone's city"
msgstr ""

#: package/contents/ui/configTimeZones.qml:125
#, kde-format
msgid "Switch Systemwide Time Zone…"
msgstr ""

#: package/contents/ui/configTimeZones.qml:137
#, kde-format
msgid "Remove this time zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:149
#, kde-format
msgid "Systemwide Time Zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:149
#, kde-format
msgid "Additional Time Zones"
msgstr ""

#: package/contents/ui/configTimeZones.qml:162
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""

#: package/contents/ui/configTimeZones.qml:174
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr ""

#: package/contents/ui/configTimeZones.qml:181
#, kde-format
msgid ""
"Using this feature does not change the systemwide time zone. When you "
"travel, switch the systemwide time zone instead."
msgstr ""

#: package/contents/ui/configTimeZones.qml:206
#, kde-format
msgid "Add More Time Zones"
msgstr ""

#: package/contents/ui/configTimeZones.qml:218
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local time zone was enabled "
"automatically."
msgstr ""

#: package/contents/ui/configTimeZones.qml:254
#, kde-format
msgid "%1, %2 (%3)"
msgstr ""

#: package/contents/ui/configTimeZones.qml:256
#, kde-format
msgid "%1, %2"
msgstr ""

#: package/contents/ui/main.qml:192
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: package/contents/ui/main.qml:196
#, kde-format
msgid "Adjust Date and Time…"
msgstr ""

#: package/contents/ui/main.qml:202
#, kde-format
msgid "Set Time Format…"
msgstr ""

#: package/contents/ui/Tooltip.qml:34
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr ""

#: package/contents/ui/Tooltip.qml:125
#, kde-format
msgctxt "@label %1 is a city or time zone name"
msgid "%1:"
msgstr ""

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr ""

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr ""

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr ""

#: plugin/timezonemodel.cpp:147
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr ""

#: plugin/timezonemodel.cpp:149
#, kde-format
msgid "System's local time zone"
msgstr ""
