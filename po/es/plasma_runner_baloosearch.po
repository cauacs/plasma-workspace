# Spanish translations for plasma_runner_baloosearchrunner.po package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2014.
# SPDX-FileCopyrightText: 2014, 2016, 2018, 2022, 2023, 2024 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_baloosearchrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-19 00:40+0000\n"
"PO-Revision-Date: 2024-03-20 12:34+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.0\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "Abrir la carpeta contenedora"

#: baloosearchrunner.cpp:84
#, kde-format
msgctxt ""
"Audio files; translate this as a plural noun in languages with a plural form "
"of the word"
msgid "Audio"
msgstr "Sonidos"

#: baloosearchrunner.cpp:86
#, kde-format
msgid "Images"
msgstr "Imágenes"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Videos"
msgstr "Vídeos"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Spreadsheets"
msgstr "Hojas de cálculo"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Presentations"
msgstr "Presentaciones"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Folders"
msgstr "Carpetas"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Documents"
msgstr "Documentos"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Archives"
msgstr "Archivos comprimidos"

#: baloosearchrunner.cpp:93
#, kde-format
msgid "Texts"
msgstr "Textos"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Files"
msgstr "Archivos"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Buscar en archivos, correo electrónico y contactos"

#~ msgid "Email"
#~ msgstr "Correo electrónico"
